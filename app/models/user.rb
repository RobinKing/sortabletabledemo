class User < ActiveRecord::Base
  include RankedModel
  ranks :row_order_position
  attr_accessible :name, :row_order_position

end
