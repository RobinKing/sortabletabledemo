class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.integer :row_order_position

      t.timestamps
    end
  end
end
