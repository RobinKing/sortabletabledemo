<h1> 这是一个Rails拖动表单排序的Demo</h1>
利用rails 实现的表单。参考
http://benw.me/posts/sortable-bootstrap-tables/
## 初始化Rails项目
新建名为SortableTable的Rails项目

    rails new SortableTable -T

编辑Gemfile,增加如下行

    gem 'ranked-model'

并执行bundle install

    bundle install

## 设置数据库
简单起见，利用scaffold生成。

    rg scaffold User name:string row_order_position:integer

并执行

    rake db:migrate

## 设置RankedModel
修改Model(user.rb)

    class User < ActiveRecord::Base
      include RankedModel
      ranks :row_order_position
      attr_accessible :name, :row_order_position
    end

修改Controller(users_controller.rb)

    def index
      @users = User.rank(:row_order_position).all
    end


## 使用bootstrap 与 jquery 美化
下载jquery与jquery-ui
并新建文件app/assets/javascripts/user/sort.js.coffee

    jQuery ->
      # this is a small hack; when a tr is dragged with jQuery UI sortable
      # the cells lose their width
      cells = $('.table').find('tr')[0].cells.length
      desired_width = 940 / cells + 'px'
      $('.table td').css('width', desired_width)
      #
      $('#sortable').sortable(
        axis: 'y'
        items: '.item'
    #
      # highlight the row on drop to indicate an update
        stop: (e, ui) ->
      ui.item.children('td').effect('highlight', {}, 600)
       update: (e, ui) ->
         item_id = ui.item.data('item_id')
          position = ui.item.index()
          $.ajax(
            type: 'POST'
            url: $(this).data('update_url')
            dataType: 'json'
    #
            # the :thing hash gets passed to @thing.attributes
            # row_order is the default column name expected in ranked-model
            data: { id: item_id, thing: { row_order_position: position } }
          )
      )

修改application.js文件：

      //= require ./user/sort

修改CSS文件(app/assets/stylesheets/users.css.scss)，增加如下内容

    #sortable {
      tr.item {
        cursor: row-resize;
      }
    }

## 增加sort方法
修改routes.rb,增加

    post "users/sort"
    root :to => "users#index"


修改Controller(users_controller.rb)，增加方法sort

    def sort
      @user = User.find(params[:id])
      #
      # .attributes is a useful shorthand for mass-assigning
      # values via a hash
      @user.attributes = params[:thing]
      #
      @user.save
      @
      # this action will be called via ajax
      render nothing: true
    end

## 修改View(users/index.html.erb)

修改表单，删除row_order_position 相关的内容
<br>
美化index

	<h1>Listing users</h1>

	<p>
	<%= link_to '新建用户', new_user_path, :class => "btn" %>
	</p>

	  <table class="table table-bordered table-striped" id="sortable" data-update_url = <%= users_sort_path%> >
	  <tr>
	    <th>Name</th>
	    <th>排序号</th>
	    <th></th>
	  </tr>

	<% @users.each do |user| %>
	  <tr data-item_id = <%= "#{user.id}" %> class = 'item' >
	    <td><%= user.name %></td>
	    <td><%= user.row_order_position %></td>
	    <td><%= link_to 'Show', user ,:class => 'btn'%></td>
	    <td><%= link_to 'Edit', edit_user_path(user), :class => "btn btn-primary" %></td>
	    <td><%= link_to 'Del', user, method: :delete, data: { confirm: 'Are you sure?' }, :class => "btn btn-danger" %></td>
	  </tr>
	<% end %>
	</table>

	<br />

## 启动

    rails s
# 存在的问题

在序号连续的时候，拖动会出问题。